This theme is for Drupal.org sites.


## Contributing

Work from the `dev` branch, using feature branches named
`{issue number}-{short-description}`, for example
`2182993-api-breadcrumb-space`.

See https://www.drupal.org/node/2406727 for more information.


### Where’s the CSS?

CSS is compiled with Compass. See https://drupal.org/node/1953368 for more
information.

Dev VMs come with a pre-compiled version, and Compass set up to compile
changes.

To compile the CSS code, navigate the the `bluecheese` folder, and run:

`$ bundle exec compass compile`

If the above doesn't work, then try this alternative commands:
```
$ /usr/bin/bundle binstubs compass
$ ./bin/compass compile
```

See https://drupal.org/node/1018084 for more information.
